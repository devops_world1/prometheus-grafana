# prometheus-grafana

## Prometheus Installation
kubectl apply -f prometheus.yaml<br>


## Grafana Installation
helm repo add grafana https://grafana.github.io/helm-charts<br>
helm repo update<br>
helm install grafana grafana/grafana<br>

helm show values grafana grafana/grafana > grafana-helmvalues.yaml<br>
helm upgrade grafana grafana/grafana --values=grafana-helmvalues.yaml<br>


